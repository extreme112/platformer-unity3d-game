﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
    GameObject player;

    float currentDist;  //absolute value of distance between player and camera
    float maxAllowedDist = 1f;
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        //Debug.Log("Max distance:" + maxAllowedDist);
	}
	
	// Update is called once per frame
	void Update () {

        currentDist = Mathf.Abs(player.transform.position.x - transform.position.x);
        //Debug.Log("Current distance:" + currentDist);
        if(currentDist > maxAllowedDist)
        {
            if(player.transform.position.x > transform.position.x)
            {
                transform.Translate(currentDist - maxAllowedDist, 0, 0);
            } else if(player.transform.position.x < transform.position.x)
            {
                transform.Translate(-(currentDist - maxAllowedDist), 0, 0);
            }
           
        }
        
        
	}
}
