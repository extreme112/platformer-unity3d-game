﻿using UnityEngine;
using System.Collections;

public class ProjectileMovement : MonoBehaviour {
    //[SerializeField]
    float projectileSpeed = 0.2f;
    
    float timeToDelete = 3;

    GameObject player;
    Transform spawnPoint_Child;
    float direction;

    float timer;

    Vector2 shootDir;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        spawnPoint_Child = player.GetComponentInChildren<Transform>();

        Vector3 mousePoint_3D = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePoint_2D = new Vector2(mousePoint_3D.x, mousePoint_3D.y);
        //Vector2 playerPos_2D = new Vector2(player.transform.position.x, player.transform.position.y);
        Vector2 playerPos_2D = new Vector2(spawnPoint_Child.position.x, spawnPoint_Child.position.y);
        shootDir = mousePoint_2D - playerPos_2D;
        
        shootDir = shootDir.normalized;
        //Debug.Log("mousePoint_3D = " + mousePoint_3D);
        //Debug.Log("mousePoint_2D = " + mousePoint_2D);
        //Debug.Log("player.transform.position = " + player.transform.position);

        timer = Time.time;
        
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(shootDir.x * projectileSpeed, shootDir.y * projectileSpeed, 0);
        DeleteTimer();
	}
        
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy") { 
            Destroy(gameObject);
            other.gameObject.GetComponent<EnemyHealth>().TakeDamage(50);
        }
    }
    void DeleteTimer() {
        if (Time.time > timer + timeToDelete)
        {
            Destroy(gameObject);
        }
    }
}
