﻿using UnityEngine;
using System.Collections;

public class ProjectileMovementEnemy : MonoBehaviour {
    Vector2 playerPos;
    Vector2 moveDir;

    float destroyTimer;
    float destroyDelay = 4;
    float moveSpeed = 0.15f;


	// Use this for initialization
	void Start () {
        playerPos = PositionTracker.Instance.GetPlayerPosition();
        Debug.Log("playerPos:" + playerPos);
        moveDir = playerPos - (Vector2)transform.position;
        Debug.Log("movDir: " + moveDir);
        moveDir = moveDir.normalized;
        Debug.Log("moveDir normalized: " + moveDir);

        destroyTimer = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if(Time.time > destroyTimer + destroyDelay)
        {
            DestroyObject(gameObject);
        }
        transform.Translate(moveDir * moveSpeed);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(20);
            Destroy(gameObject);
        }
    }
}
