﻿using UnityEngine;
using System.Collections;

public class PositionTracker : MonoBehaviour {

    public GameObject player;
    Vector2 currentPosition;
    public static PositionTracker Instance { get; private set; }

    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
        DontDestroyOnLoad(gameObject);
        currentPosition = player.transform.position;
    }

    void Update()
    {
        currentPosition = player.transform.position;
    }

    public Vector2 GetPlayerPosition() { 
        return currentPosition;
    }
}
