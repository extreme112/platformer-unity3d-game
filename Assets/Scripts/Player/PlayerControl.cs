﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    Rigidbody2D rb;

    //Movement
    [SerializeField]
    float moveSpeed_normal = 100f;
    [SerializeField]
    float moveSpeed_power = 150f;
    [SerializeField]
    float moveSpeedVel_max = 5;
    float moveSpeed;

    Vector2 vel;

    //Jumping
    [SerializeField]
    float jumpSpeed_normal = 100;
    //[SerializeField]
    float jumpSpeed_power = 150;
    float jumpSpeed;

    RaycastHit2D hit;
    //Animation
    Animator anim;
    //PowerUps
    float powerTimerMoveSpeed = 0;
    float powerTimerJumpSpeed = 0;

    void Start() {
        anim = GetComponent<Animator>();
        moveSpeed = moveSpeed_normal;
        jumpSpeed = jumpSpeed_normal;
        rb = GetComponent<Rigidbody2D>();
        vel = Vector2.right * moveSpeed;
    }

	void Update () {
        //SpriteChanging();
        Movement();
        //Shooting();
        Jumping();
        if (powerTimerMoveSpeed >= 0)
        {
            powerTimerMoveSpeed -= Time.deltaTime;
            if (powerTimerMoveSpeed <= 0)
            {
                moveSpeed = moveSpeed_normal;
                //Debug.Log("PowerUpMoveSpeed Expired");
            }
        } else if (powerTimerJumpSpeed >= 0)
        {
            powerTimerJumpSpeed -= Time.deltaTime;
            if(powerTimerMoveSpeed <= 0)
            {
                jumpSpeed = jumpSpeed_normal;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Increase movement speed powerup
        if (other.gameObject.CompareTag("PowerUpMove")) {
            Debug.Log("PowerUpMoveSpeed Touched");
            powerTimerMoveSpeed = 10;
            moveSpeed = moveSpeed_power;
            Destroy(other.gameObject);
        } else if (other.gameObject.CompareTag("PowerUpJump"))
        {
            Debug.Log("PowerUpJumpSpeed Touched");
            powerTimerJumpSpeed = 10;
            jumpSpeed = jumpSpeed_power;
            Destroy(other.gameObject);
        }
    }
 
    void Movement() {
        //Movement
        if (Input.GetKey("right"))
        {
            if (Mathf.Abs(rb.velocity.x) < moveSpeedVel_max)
                rb.AddForce(vel, ForceMode2D.Force);
            anim.SetBool("IsWalking", true);
            if (transform.localScale.x < 0)
            {
                Vector3 scale = transform.localScale;
                scale.x *= -1;
                transform.localScale = scale;
            }
        }
        else if (Input.GetKey("left"))
        {
            if (Mathf.Abs(rb.velocity.x) < moveSpeedVel_max)
                rb.AddForce(-vel, ForceMode2D.Force);
            anim.SetBool("IsWalking", true);
            if (transform.localScale.x > 0)
            {
                Vector3 scale = transform.localScale;
                scale.x *= -1;
                transform.localScale = scale;
            }
        }
        else
        {
            anim.SetBool("IsWalking", false);
        }
    }
    void Jumping() {
        //Jumping
        if (Input.GetKeyDown("up"))
        {
            hit = Physics2D.Raycast(transform.position, Vector2.down,1f);
            Debug.Log("Up pressed");
            if (hit.collider != null && hit.collider.tag == "Floor")
            {
                Debug.Log("Floor below" + hit.collider.tag);
                SoundManager.Instance.PlayClip("Jump");
                rb.AddForce(new Vector2(0, jumpSpeed), ForceMode2D.Impulse);
            }
        }
    }

   
}
