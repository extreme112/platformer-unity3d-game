﻿//Controls the health and damage system in relation to the player.
//Also deals with death event
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerHealth : MonoBehaviour {
    public Slider healthSlider;
    public GameObject gameController_GO;
    Vector3 spnPnt;

    int startingHealth = 100;
    int currentHealth;

	// Use this for initialization
	void Start () {
        currentHealth = startingHealth;
        healthSlider.maxValue = startingHealth;
        healthSlider.value = startingHealth;
        // gameController = gameController_GO.GetComponent<GameController>();
        spnPnt = gameController_GO.GetComponent<GameController>().GetPlayerSpawnPoint();

	}
	
	// Update is called once per frame
	void Update () {
        
	}

    //Subtracts dmg from currenthealth and updates the healthSlider
    public void TakeDamage(int dmg)
    {
        if (currentHealth > 0)
        {
            currentHealth -= dmg;
            healthSlider.value = currentHealth;
        }

        if (currentHealth <= 0) {
            //reload current scene
            this.GetComponent<PlayerControl>().enabled = false;
            currentHealth = 100;
            Time.timeScale = 0;
            StartCoroutine(ExecuteAfterTime(4));
            
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Trigger touched - PlayerHealth");
        if(other.tag == "Checkpoint")
        {
            Debug.Log("Checkpoint touched!");
            spnPnt = other.transform.position;
        }

    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        transform.position = gameController_GO.GetComponent<GameController>().GetPlayerSpawnPoint();
        this.GetComponent<PlayerControl>().enabled = true;
        Time.timeScale = 1;
    }

}
