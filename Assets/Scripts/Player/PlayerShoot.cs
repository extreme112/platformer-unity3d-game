﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

    //////////////////////////////////////////////
    public Transform spawnPoint;
    public GameObject projectile;
    //Shootin
    float shootDelay = 0.5f;
    float timer;

    // Use this for initialization
    void Start () {
        timer = Time.time;	
	}
	
	// Update is called once per frame
	void Update () {
        Shooting();
	}

    void Shooting()
    {
        //Shoot event
        if (Input.GetMouseButton(0))
        {
            if (Time.time > timer + shootDelay)
            {
                Instantiate(projectile, spawnPoint.position, Quaternion.identity);
                SoundManager.Instance.PlayClip("Bullet1_small");
                timer = Time.time;
            }
        }
    }
}
