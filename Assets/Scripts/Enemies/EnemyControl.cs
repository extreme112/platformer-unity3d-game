﻿using UnityEngine;
using System.Collections;

public class EnemyControl : MonoBehaviour {
    int damage = 20;
    float dmgDelay = 2;
    float timer;
	// Use this for initialization
	void Start () {
        timer = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
	}


    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(Time.time > timer + dmgDelay)
            {
                other.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
                timer = Time.time;
            }
        }
        
    }
}
