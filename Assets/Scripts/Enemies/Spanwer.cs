﻿using UnityEngine;
using System.Collections;

public class Spanwer : MonoBehaviour {
    public GameObject enemy2Spawn;

    float x1 = -6.76f;
    float x2 = 10.35f;
    float y1 = -2.0f;
    float y2 = 0f;

    int counter = 51;
    int timer = 50;

    int enemies2Spawn = 10;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if(enemies2Spawn == 0)
        {
            Destroy(gameObject);
        }

	    if(counter > timer)
        {
            float xSpwnPoint = Random.Range(x1, x2);
            float ySpwnPoint = Random.Range(y1, y2);
            Vector2 spwnPoint = new Vector2(xSpwnPoint, ySpwnPoint);
            Instantiate(enemy2Spawn, spwnPoint,Quaternion.identity);
            enemies2Spawn--;

            counter = 0;
        }

        counter++;
	}
}
