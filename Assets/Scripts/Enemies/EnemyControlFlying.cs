﻿using UnityEngine;
using System.Collections;

public class EnemyControlFlying : MonoBehaviour {

    float moveTimer;
    float shootTimer;
    Vector2 movDir;
    float moveDelay = 2; //time in seconds till object switches horizontal direction
    float shootDelay = 1;

    public GameObject projectile;
	// Use this for initialization
	void Start () {
        moveTimer = Time.time;
        shootTimer = Time.time;
        movDir = new Vector2(0.10f, 0);
	}
	
	// Update is called once per frame
	void Update () {
        Movement();
        Shoot();
	}

    void Movement() {
        if (Time.time > moveTimer + moveDelay)
        {
            movDir = -movDir;
            moveTimer = Time.time;
        }

        transform.Translate(movDir);
    }

    void Shoot()
    {
        if(Time.time > shootTimer + shootDelay)
        {
            Instantiate(projectile, transform.position, Quaternion.identity);
            shootTimer = Time.time;
        }
            
    }
}
