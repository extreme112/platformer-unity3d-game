﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
    int health = 100;

    public void TakeDamage(int dmg)
    {
        health -= dmg;
        if(health <= 0)
        {
            SoundManager.Instance.PlayClip("EnemyDissapear");
            Destroy(gameObject);
        }
    }
}
