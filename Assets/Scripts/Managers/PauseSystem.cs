﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PauseSystem : MonoBehaviour {
    public GameObject pauseMenu;
    public GameObject userUI;
    bool isPaused = false;
	// Use this for initialization
	void Start () {
        pauseMenu.SetActive(false);
        userUI.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
        PauseMenu();
	}

    void PauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == false)
            {
                isPaused = true;
                pauseMenu.SetActive(true);
                userUI.SetActive(false);
                Time.timeScale = 0;
            }
            else
            {
                isPaused = false;
                pauseMenu.SetActive(false);
                userUI.SetActive(true);
                Time.timeScale = 1;
            }
        }
    }

    public void ResumeGame()
    {
        SoundManager.Instance.PlayClip("Click1");
        isPaused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
