﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
    public int level_indicator;
    GameObject player;
    Vector3 spawnPoint;


	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        spawnPoint = player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public int GetLevelIndicator()
    {
        return level_indicator;
    }

    public Vector2 GetPlayerSpawnPoint()
    {
        return spawnPoint;
    }

    public void SetPlayerSpawnPoint(Vector2 newspawn)
    {
        spawnPoint = newspawn;
    }
}
