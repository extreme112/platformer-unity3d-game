﻿using UnityEngine;
using System.Collections;

public class EndOfLevelUIMenuManager : MonoBehaviour {
    public GameObject endoflevelcanvas;
    public GameObject userUI;
    public GameObject twoChainz;
	// Use this for initialization
	void Start () {
        endoflevelcanvas.SetActive(false);
        twoChainz.SetActive(false);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Player touched house");
        if(other.gameObject.tag == "Player")
        {
            userUI.SetActive(false);
            endoflevelcanvas.SetActive(true);
            twoChainz.SetActive(true);
        }
    }
}
