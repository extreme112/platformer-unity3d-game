﻿//This script contains all the functions needed to transition between scenes.
//Levels in this game are named in this conventions: Level1, Level2, Level3 etc.
//A public int called level_indicator is added to indicated which level the GameObject this script
//is attatched to.  Mainly used for GoToNextLevel and Retry()

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public int level_indicator;
    
    //Loads menu scene.
    public void GoToMainMenu()
    {
        SoundManager.Instance.PlayClip("Click2");
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;   
    }

    //Loads the LevelSelect scene.
    public void GoToLevelSelect()
    {
        SoundManager.Instance.PlayClip("Click1");
        SceneManager.LoadScene("LevelSelect");
        Time.timeScale = 1;
    }

    //Takes in a the level number the at should be loaded via string.
    //Loads the level of the number entered.
    public void LoadLevel(string levelNumber)
    {
        SoundManager.Instance.PlayClip("Click1");
        string name = "Level" + levelNumber;
        SceneManager.LoadScene(name);
        Time.timeScale = 1;
    }

    //Loads the scene of level_indicator, which happens to be the same level.
    //Reloads current level.
    public void Retry()
    {
        SoundManager.Instance.PlayClip("Click1");
        string name = "Level" + level_indicator.ToString();
        SceneManager.LoadScene(name);
        Time.timeScale = 1;
    }

    //Uses level_indicator to load the next level.
    public void GoToNextLevel()
    {
        SoundManager.Instance.PlayClip("Click1");
        string name = "Level" + (level_indicator + 1).ToString();
        SceneManager.LoadScene(name);
        Time.timeScale = 1;
    }

    public void ClearPlayerPrefs() {
        SoundManager.Instance.PlayClip("Click1");
        PlayerPrefs.DeleteAll();
    }

    public void ExitApplication()
    {
        SoundManager.Instance.PlayClip("Click1");
        Application.Quit();
    }
}
