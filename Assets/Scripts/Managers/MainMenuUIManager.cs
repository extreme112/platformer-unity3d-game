﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuUIManager : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject playerPrefsMenu;

    void Start()
    {
        if (playerPrefsMenu.activeInHierarchy)
            playerPrefsMenu.SetActive(false);
    }

    public void OptionsMenu(bool clicked) {
        if (clicked)
        {
            mainMenu.SetActive(!clicked);
            optionsMenu.SetActive(true);
        } else
        {
            mainMenu.SetActive(!clicked);
            optionsMenu.SetActive(false);
        }
    }

    public void ClearPlayerPrefs()
    {
        mainMenu.SetActive(false);
        playerPrefsMenu.SetActive(true);
        SoundManager.Instance.PlayClip("Click1");
    }

    public void ClearPlayerPrefs_No()
    {
        SoundManager.Instance.PlayClip("Click1");
        mainMenu.SetActive(true);
        playerPrefsMenu.SetActive(false);
        
    }

    public void ClearPlayerPrefs_Yes()
    {
        mainMenu.SetActive(true);
        playerPrefsMenu.SetActive(false);
        SoundManager.Instance.PlayClip("Click1");
        PlayerPrefs.DeleteAll();
    }

    
}
