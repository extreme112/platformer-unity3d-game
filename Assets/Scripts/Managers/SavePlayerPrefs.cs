﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SavePlayerPrefs : MonoBehaviour {

    int HighestLevelCompleted;
    int level_indicator;


    // Use this for initialization
    void Start () {
        HighestLevelCompleted = PlayerPrefs.GetInt("HighestLevelCompleted");
        level_indicator = SceneManager.GetActiveScene().buildIndex - 1;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
       if(level_indicator > HighestLevelCompleted)
        {
            PlayerPrefs.SetInt("HighestLevelCompleted", level_indicator);
        }
    }

    
}
