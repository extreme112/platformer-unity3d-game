﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelXInteractable : MonoBehaviour {

    public int connected_level;
    int HighestLevelCompleted;
    Button thisButton;

	// Use this for initialization
	void Start () {
        thisButton = GetComponent<Button>();

        HighestLevelCompleted = PlayerPrefs.GetInt("HighestLevelCompleted");
	    if(!(connected_level - 1 <= HighestLevelCompleted))
        {
            thisButton.interactable = false;
        }
        // else do nothing.
	}
	
	// Update is called once per frame
	void Update () {
        HighestLevelCompleted = PlayerPrefs.GetInt("HighestLevelCompleted");
        if (connected_level - 1 <= HighestLevelCompleted) {
            thisButton.interactable = true;
        }
	}
}
