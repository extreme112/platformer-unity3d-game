﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sound
{
    public string _name;
    public AudioClip clip;
}

public class SoundManager : MonoBehaviour {
    [SerializeField]
    Sound[] sounds;
    // Use this for initialization

    public static SoundManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }

        // Here we save our singleton instance
        Instance = this;

        DontDestroyOnLoad(gameObject);
    }
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayClip(string _name)
    {
        for(int i = 0; i < sounds.Length; i++)
        {
            if(_name == sounds[i]._name)
            {
                AudioSource.PlayClipAtPoint(sounds[i].clip, Vector2.zero,1.0f);
                return;
            }
        }

        Debug.Log("No sound found for " + _name + ".");
    }
}
