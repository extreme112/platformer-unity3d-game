﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {
    public GameObject gameController;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")){
            gameController.GetComponent<GameController>().SetPlayerSpawnPoint(transform.position);
        }
    }
}
